(ns my-project.client
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require
   [reagent.core :as r]
   [cljs-http.client :as http]
   [cljs.core.async :refer [<!]]))

(def app-state
  (r/atom
   {:results []
    :results-page 0
    :search-input ""
    :cart []
    :cart-hidden true 
    :checkout-hidden true}))

(defn remove-from-cart-handler [e]
  (.preventDefault e)
  (swap! app-state assoc :cart 
          (doall
           (remove
            (fn [video-game-result]
              (= (str (get video-game-result :id))
                 e.target.dataset.vgid))
            (@app-state :cart)))))

(defn add-to-cart-handler [e]
  (.preventDefault e)

  (swap! app-state assoc :cart
         (conj (:cart @app-state)
               (first (filter
                       (fn [video-game-result]
                         (= (str (get video-game-result :id))
                            e.target.dataset.vgid))
                       (@app-state :results))))))

(defn giant-bomb-search-update! [query page]
  (go
    (let [response
          (<!
           (http/jsonp
            "https://www.giantbomb.com/api/search"
            {:callback-name "json_callback"
             :query-params
             {"api_key" "65941e2952105bf62ba0ac1ea41296b52f1fcead"
              "format" "jsonp"
              "page" page
              "resources" "game"
              "query" query
              }}))]
      (swap! app-state assoc :results
              (into (@app-state :results)
                    (get-in response [:body :results]))))))

(defn result-component [results]
  (if (not= results [])
    [:div {:id "results"}
     (for [result results]
       [:div { :key (get result :id) :result result :class "result"}
        [:div {:class "media"}
         [:img { :class "thumb" :src (get-in result [:image :thumb_url]) }]]
        [:div {:class "text"}
         [:h2 {:class "title"} (get result :name)]
         [:span {:class "description"} (get result :deck)]
         [:button { :on-click add-to-cart-handler
                   :data-vgid (get result :id)} "add to rental cart" ]]])
     [:button {:id "load-more-button" :style {  :margin-top "20px"}}
      [:span
       {:on-click
        (fn [e]
          (swap! app-state assoc :results-page
                 (inc (@app-state :results-page)))
          (giant-bomb-search-update!
           (@app-state :search-input)
           (@app-state :results-page)))}
       "load more results..."] ]]))

(defn search-bar-click-handler [e]
  (swap! app-state assoc :results-page 0)
  (swap! app-state assoc :results [])
  (giant-bomb-search-update!
   (@app-state :search-input)
   (@app-state :results-page)))

(defn cart-click-handler []
  (swap! app-state assoc :cart-hidden (not (@app-state :cart-hidden))))

(defn checkout-click-handler [e]
  (swap! app-state assoc :cart-hidden true)
  (swap! app-state assoc :checkout-hidden (not (@app-state :checkout-hidden))))

(defn checkout-component []
  [:div
   {:id "checkout"
    :class (if (@app-state :checkout-hidden) "checkout-closed" "checkout-open")}
   [:div
    {:id "checkout-container"}
    [:h2 "Checkout"]
    [:div [:div {:class "label"} "first name"] [:input {:type "text"}]]
    [:div [:div {:class "label"} "last name"] [:input {:type "text"}]]
    [:div
     [:div {:id "rental-agreement"}
      "I pinky swear that I will return these video games in working
      condition as soon as I am done with them."]
     [:input {:type "checkbox"}]
     [:div {:class "label"
            :style {:display "inline-block" :transform "translateY(-4px)"}}
      "pinky swear?"] ]
    [:button {:id "checkout-submit" :type "submit"} "checkout!"]]])

(defn cart-component []
  [:div
   [:div
    {:id "cart-contents"
     :class (if (@app-state :cart-hidden) "cart-closed" "cart-open")}
    (for [result (@app-state :cart) ]
      [:div { :key (get result :id) :result result :class "result"}
       [:img { :class "thumb" :src (get-in result [:image :tiny_url]) }]
       [:span {:class "title"} (get result :name)]
       [:button
        { :class "remove-button"
         :on-click remove-from-cart-handler
         :data-vgid (get result :id)}
        "remove" ]
       ])]
   [:div
    {:id "cart" }
    [:div {:id "cart-container"}
     [:button
      { :on-click cart-click-handler :style
       { :padding "15px"  :font-weight "bold"}}
      "cart"
      [:div 
       {:id "cart-count" }
       (count (@app-state :cart))]]
     (if (@app-state :checkout-hidden)
       [:button
        { :id "checkout-toggle-button"
         :on-click checkout-click-handler
         :style { :background-color "rgb(98, 154, 129)"}}
        "proceed to checkout >"]
       [:button
        { :id "checkout-toggle-button"
         :on-click checkout-click-handler
         :style { :background-color "transparent"}}
        "< back to search" ])]]])

(defn search-button []
  [:button
   {:on-click search-bar-click-handler :id "search-button"}
   "search"])

(defn search-bar []
  [:input {:type "text"
           :on-change
           (fn [a] (swap! app-state assoc :search-input a.target.value))
           :id "search-bar"
           :placeholder "type a game name here..."}])

(defn root-component []
  [:div {:style {}}
   [:h1 {:id "logo"}
    [:svg {:viewbox "0 0 90 31.1", :id "shape-gravie-logo",
           :width "95px", :height "39px"
           :style { :fill "white" :transform "scale(2)"  }}
     [:title "gravie-logo"]
     [:path {:d "M6.8,31.1c-4.5,0-6.8-3.3-6.8-6.3V6.3C0,3.3,2.3,0,6.8,0c4.8,0,6.5,3.3,6.5,6v3.7h-1.7V6.6\n\tc0-1.7-1.2-4.8-4.9-4.8c-3.7,0-5.1,3.1-5.1,5v17.5c0,1.9,1.4,5,5.1,5c3.7,0,4.9-3.1,4.9-4.8v-8.1H6.1v-1.7h7.3V25\n\tC13.4,27.7,11.6,31.1,6.8,31.1 M90,6c0-2.7-1.7-6-6.5-6c-4.5,0-6.8,3.3-6.8,6.3v18.5c0,3,2.3,6.3,6.8,6.3c4.8,0,6.5-3.3,6.5-6v-3.7\n\th-1.7v3.1c0,1.7-1.2,4.8-4.9,4.8c-3.7,0-5.1-3.1-5.1-5V6.8c0-1.9,1.4-5,5.1-5c3.7,0,4.9,3.1,4.9,4.8v8.1h-5.6v1.7H90V6 M25.8,16.4\n\tc1.9,0,3.3,0.6,3.3,3v11h1.7V18.9c0-1.9-1.1-3.3-2.3-3.8c1.2-0.7,2.3-1.4,2.3-4.4V5.3c0-2.7-1.3-4.6-4.4-4.6h-8.4v29.7h1.7v-28h5.9\n\tc2.8,0,3.5,1.4,3.5,3.5v5.1c0,2.5-1.6,3.7-3.3,3.7h-1.7v1.8H25.8z M65.8,0.7h-1.7L61.7,27c-0.1,1.2-0.9,2.2-2.7,2.2\n\tc-1.4,0-2.3-1.2-2.4-2.2L54.1,0.7h-1.7L55,27.6c0.1,0.8,0.6,3.5,4.1,3.5c3.7,0,4.3-2.4,4.4-3.5L65.8,0.7z M35.5,31.1h1.7l2.4-26.3\n\tc0.1-1.2,0.9-2.2,2.7-2.2c1.4,0,2.3,1.2,2.4,2.2l2.5,26.3h1.7L46.4,4.2c-0.1-0.8-0.6-3.5-4.1-3.5c-3.7,0-4.3,2.4-4.4,3.5L35.5,31.1z\n\t M69.9,30.4h1.7V0.7h-1.7V30.4z"}]" "]

    [:h3 {:id "service-description" :style
          {:font-size "14px"
           :color "white" :margin 0
           :padding "10px 0 10px 0"}}
     "video game rental service"]]
   [:div 
    {:id "search" }
    (search-bar)
    (search-button)] 
   (if (> (count (@app-state :cart)) 0) [cart-component])
   [checkout-component]
   [result-component  (@app-state :results)]])

(defn ^:export mount-root []
  (r/render [root-component]
            (.getElementById js/document "app")))
